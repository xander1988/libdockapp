#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/Xproto.h>
#include <X11/xpm.h>
#include <X11/extensions/shape.h>
#include <stdbool.h>

#define DA_MAX_MOUSE_REGION 50
#define DA_MAX_COLORS       20
#define CENTER_TEXT         -5

enum {
    EXIT,
    EXPOSE,
    MOUSE_1_PRS, /* left pressed */
    MOUSE_2_PRS, /* middle pressed */
    MOUSE_3_PRS, /* right pressed */
    MOUSE_1_REL, /* left released */
    MOUSE_2_REL, /* middle released */
    MOUSE_3_REL, /* right released */
    MOUSE_4,     /* wheel scrolls up */
    MOUSE_5      /* wheel scrolls down */
};

typedef struct {
    bool enable;
    int top;
    int bottom;
    int left;
    int right;
} DAMouseRegion;

typedef struct {
    Pixmap pixmap;
    Pixmap mask;
    XpmAttributes attributes;
} DAXpmIcon;

Window da_root_win;
Window da_icon_win, da_win;
Window da_main_win;
Window da_active_win;

Display *da_display;

XEvent da_xevent;

int da_display_depth;
int da_screen;
int da_x_fd;

XpmColorSymbol da_colors[DA_MAX_COLORS];

/* Prototypes */
void da_add_mouse_region(int, int, int, int, int);

void da_enable_mouse_region(int);

void da_disable_mouse_region(int);

int da_check_mouse_region(void);

void da_init_xwindow(void);

void da_open_xwindow(int, char **, char **, unsigned char *, int, int);

void da_redraw_window(void);

void da_redraw_window_xy(int, int);

void da_redraw_window_area(int, int, int, int, int, int);

void da_fill_xpm_rectangle(Pixmap, int, int, int, int);

void da_draw_xpm_rectangle(Pixmap, int, int, int, int);

void da_fill_rectangle(int, int, int, int);

void da_draw_rectangle(int, int, int, int);

void da_set_foreground(int);

void da_copy_wm_area(int, int, int, int, int, int);

void da_copy_xpm_area(int, int, int, int, int, int);

void da_copy_from_xpm(Pixmap, int, int, int, int, int, int);

void da_copy_from_xpm_to_xpm(Pixmap, Pixmap, int, int, int, int, int, int);

void da_copy_xpm_area_with_trans(int, int, int, int, int, int);

void da_copy_from_xpm_with_trans(Pixmap, int, int, int, int, int, int);

void da_copy_xpm_to_xpm_with_trans(Pixmap, Pixmap, int, int, int, int, int, int);

/* To be removed */
void da_combine_xpm_with_trans(Pixmap, Pixmap, int, int, int, int, int, int);

/* To be removed */
void da_combine_with_trans(int, int, int, int, int, int);

/* To be removed */
void da_copy_xbm_area(int, int, int, int, int, int);

void da_set_mask(Pixmap, int, int, int, int, int, int);

void da_set_mask_xy(int, int);

/* To be removed (?) */
void da_create_xbm_from_xpm(char *, char **, int, int);

/* To be removed (?) */
void da_create_bitmap_data_from_pixmap(Pixmap);

unsigned long da_get_color(char *);

unsigned long da_get_blended_color(char *, int, int, int, float);

unsigned long da_mix_color(char *, int, char *, int);

void da_add_color(char *, char *);

void da_add_mixed_color(char *, int, char *, int, char *);

void da_add_blended_color(char *, int, int, int, float, char *);

int da_watch_xevent(void);

void da_draw_string(int, int, char *, char *);

void da_read_file_to_pixmap(char *, Pixmap, Pixmap);

char **da_scale_image(char **, int, int);
