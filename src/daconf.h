#include <stdio.h>
#include <string.h>      /* strcmp */
#include <ctype.h>       /* toupper */
#include <sys/stat.h>    /* stat */
#include <stdlib.h>      /* malloc */
#include <stdbool.h>
#include <unistd.h>      /* chdir */
#include <time.h>
#include <errno.h>
#include <stdarg.h>      /* opt args */
#include <X11/Xlib.h>    /* XCloseDisplay */

#define DA_WRITE_LOG
#define DA_GENERAL_CONFIG ".config/dockapps/general.conf"
#define DA_MAX_OPTIONS    50
#define DA_MAX_CONF_FILES  5    /* global conf, indie conf, alternative conf... */

typedef enum {
    INFO,
    WARN,
    ERR
} DALogLevel;

typedef enum {
    SYSTEM,
    LOCAL
} DALocation;

typedef enum {
    STRING,
    INTEGER,
    FLOATING,
    BOOLEAN
} DAType;

struct DAOption {
    char *name;
    char *key;
    char *long_key;
    DAType type;
    char *descr;
    /* modifiable values */
    char   **str_val;
    int    *int_val;
    double *flt_val;
    bool   *bool_val;
    /* default values */
    char   *def_str_val;
    int    def_int_val;
    double def_flt_val;
    bool   def_bool_val;
    /* min and max allowed values */
    int min_alld_int;
    int max_alld_int;
    double min_alld_flt;
    double max_alld_flt;
} da_option[DA_MAX_OPTIONS];

struct DAConfig {
    char *path;
    time_t last_mod_time;
} da_config[DA_MAX_CONF_FILES];

extern Display *da_display;

struct stat da_filestat;

char *da_name;        /* dockapp name */
char *da_version;     /* dockapp version */
char *da_descr;       /* dockapp description */
char *da_log_file;    /* resides in home dir */

/* low-level options */
int da_scale;           /* dockapp size; 1 is for 64x64, 2 is for 128x128, etc */
char *da_display_name;
char *da_geometry;
char *da_back_image;
bool da_showhelp;
bool da_showver;
bool da_windowed;
bool da_dock_win;

void da_print_usage(void);
void da_print_version(void);
void da_parse_all(int, char **);
void da_parse_file(char *);
void da_parse_args(int, char **);
bool da_parse_bool_option(char *);
void da_insert_value(int, char *);
bool da_conf_changed(int, char **);
void da_reset_to_defaults(void);
void da_init_main(char *, char *, char *);
void da_init_opt_info(char *, char *, char *, char *);
void da_init_string(char **, char *, char *, char *, char *, char *);
void da_init_integer(int *, char *, char *, char *, int, int, int, char *);
void da_init_float(double *, char *, char *, char *, double, double, double, char *);
void da_init_bool(bool *, char *, char *, char *, bool, char *);
void da_init_conf_file(DALocation, char *);
void da_parse_conf_files(void);
void da_log(DALogLevel, char *, ...);
