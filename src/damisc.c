#include "damisc.h"

static void sigchldhandler(int);

static void sigchldhandler(int a) {
    (void)a;
    
    wait(NULL);

    da_exec_busy = false;
}

bool da_exec_command(char *command) {
    pid_t pid;

    signal(SIGCHLD, sigchldhandler);

    if (da_exec_busy) {
        return true;
    }

    da_exec_busy = true;

    if ((pid = fork()) == 0) {
        if (system(command) == -1) {
            return false;
        }
        
        exit(10);
    }
    
    return true;
}

bool da_timeout(int seconds) {
    static time_t stamp = 0;
    
    if (((time(NULL) - stamp) > seconds) || (!stamp)) {
        stamp = time(NULL);

        return true;
    }

    return false;

    /*static clock_t stamp = 0;
    
    clock_t msec;

    msec = (clock_t)(seconds * 1000 / CLOCKS_PER_SEC);
    
    if (((clock() * 1000 / CLOCKS_PER_SEC) - stamp > msec) || (!stamp)) {
        stamp = clock() * 1000 / CLOCKS_PER_SEC;

        return true;
    }
    
    return false;*/
}

char *da_str_to_upper(char *string) {
    for (int m = 0; m < (int)strlen(string); m++)
        string[m] = toupper(string[m]);

    return string;
}
