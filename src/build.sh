#!/bin/bash

if [[ ! $1 ]]; then
    if grep -Fxq "/*#define DA_WRITE_LOG*/" daconf.h; then
        sed -i 's/\/\*#define DA_WRITE_LOG\*\//#define DA_WRITE_LOG/g' daconf.h;
    fi
elif [[ $1 == "--disable-log" ]]; then
    if ! grep -Fxq "/*#define DA_WRITE_LOG*/" daconf.h; then
        sed -i 's/#define DA_WRITE_LOG/\/\*#define DA_WRITE_LOG\*\//g' daconf.h;
    fi
fi

gcc -c -fpic -Wall -Wextra daconf.c daxutils.c damisc.c;
gcc -shared daconf.o daxutils.o damisc.o -o libdockapp4.so;
