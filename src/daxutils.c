/*
 *   xutils.c - A collection of X-windows utilties for creating WindowMAker
 *      DockApps.
 *
 *     This file contains alot of the lower-level X windows routines. Origins with wmppp
 *     (by  Martijn Pieterse (pieterse@xs4all.nl)), but its been hacked up quite a bit
 *     and passed on from one new DockApp to the next.
 *
 *
 *
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2, or (at your option)
 *      any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program (see the file COPYING); if not, write to the
 *      Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *      Boston, MA 02110-1301 USA
 *
 *
 * $Id: xutils.c,v 1.2 2002/09/15 14:31:41 ico Exp $
 *
 *
 */

#include "daxutils.h"
#include "daconf.h"

DAMouseRegion mouse_region[DA_MAX_MOUSE_REGION];

Atom _XA_GNUSTEP_WM_FUNC;
Atom deleteWin;
Atom winType;
Atom dockWin;

XSizeHints mysizehints;

XWMHints mywmhints;

Pixel back_pix, fore_pix;

GC NormalGC;

DAXpmIcon wmgen;

Pixmap pixmask;
Pixmap pixmask_tile;
Pixmap pm_main;
Pixmap pm_tile;
Pixmap pm_disp;

static void da_open_normal_win(int, char **, char **, unsigned char *, int, int);
static void da_open_withdr_win(int, char **, char **, unsigned char *, int, int);

void da_add_color(char *hex_color, char *symbolic_name) {
    int number = 0;

start_over:
    if (!da_colors[number].name) {
        da_colors[number].name = malloc(strlen(symbolic_name) + 1);
        sprintf(da_colors[number].name, "%s", symbolic_name);

        da_colors[number].pixel = da_get_color(hex_color);
        
        /*da_colors[number].value = malloc(strlen(hex_color) + 1);
        sprintf(da_colors[number].value, "%s", hex_color);*/
        
    } else {
        number++;

        if (number == DA_MAX_COLORS) {
            printf("WARN: color \"%s\" cannot be initialized (max colors of %i reached)!\n", symbolic_name, DA_MAX_COLORS);
            da_log(WARN, "color \"%s\" cannot be initialized (max colors of %i reached)!\n", symbolic_name, DA_MAX_COLORS);

            return;
        }

        goto start_over;
    }
}

void da_add_mixed_color(char *hex_color1, int prop1, char *hex_color2, int prop2, char *symbolic_name) {
    int number = 0;

start_over:
    if (!da_colors[number].name) {
        da_colors[number].name = malloc(strlen(symbolic_name) + 1);
        sprintf(da_colors[number].name, "%s", symbolic_name);

        da_colors[number].pixel = da_mix_color(hex_color1, prop1, hex_color2, prop2);
        
        /*da_colors[number].value = malloc(strlen(hex_color) + 1);
        sprintf(da_colors[number].value, "%s", hex_color);*/
    } else {
        number++;

        if (number == DA_MAX_COLORS) {
            printf("WARN: color \"%s\" cannot be initialized (max colors of %i reached)!\n", symbolic_name, DA_MAX_COLORS);
            da_log(WARN, "color \"%s\" cannot be initialized (max colors of %i reached)!\n", symbolic_name, DA_MAX_COLORS);

            return;
        }

        goto start_over;
    }
}

void da_add_blended_color(char *hex_color1, int r, int g, int b, float fac, char *symbolic_name) {
    int number = 0;

start_over:
    if (!da_colors[number].name) {
        da_colors[number].name = malloc(strlen(symbolic_name) + 1);
        sprintf(da_colors[number].name, "%s", symbolic_name);

        da_colors[number].pixel = da_get_blended_color(hex_color1, r, g, b, fac);
        
        /*da_colors[number].value = malloc(strlen(hex_color) + 1);
        sprintf(da_colors[number].value, "%s", hex_color);*/
    } else {
        number++;

        if (number == DA_MAX_COLORS) {
            printf("WARN: color \"%s\" cannot be initialized (max colors of %i reached)!\n", symbolic_name, DA_MAX_COLORS);
            da_log(WARN, "color \"%s\" cannot be initialized (max colors of %i reached)!\n", symbolic_name, DA_MAX_COLORS);

            return;
        }

        goto start_over;
    }
}

unsigned long da_get_color(char *color_name) {
    XColor Color;

    if (!XParseColor(da_display, DefaultColormap(da_display, DefaultScreen(da_display)), color_name, &Color))
        fprintf(stderr, "can't parse color %s\n", color_name), exit(1);

    if (!XAllocColor(da_display, DefaultColormap(da_display, DefaultScreen(da_display)), &Color)) {
        fprintf(stderr, "can't allocate color %s. Using black\n", color_name);
        
        return BlackPixel(da_display, DefaultScreen(da_display));
    }

    return Color.pixel;
}

unsigned long da_get_blended_color(char *color_name, int r, int g, int b, float fac) {
    XColor Color;

    if ((r < -255 || r > 255) || (g < -255 || g > 255) || (b < -255 || b > 255)) {
        fprintf(stderr, "r:%d,g:%d,b:%d (r,g,b must be 0 to 255)", r, g, b);

        exit(1);
    }

    r *= 255;
    g *= 255;
    b *= 255;

    if (!XParseColor(da_display, DefaultColormap(da_display, DefaultScreen(da_display)), color_name, &Color))
        fprintf(stderr, "can't parse color %s\n", color_name), exit(1);

    if (!XAllocColor(da_display, DefaultColormap(da_display, DefaultScreen(da_display)), &Color)) {
        fprintf(stderr, "can't allocate color %s. Using black\n", color_name);

        return BlackPixel(da_display, DefaultScreen(da_display));
    }

    if (DefaultDepth(da_display, DefaultScreen(da_display)) < 16)
        return Color.pixel;

    /* red */
    if (Color.red + r > 0xffff)
        Color.red = 0xffff;
    else if (Color.red + r < 0)
        Color.red = 0;
    else
        Color.red = (unsigned short)(fac * Color.red + r);

    /* green */
    if (Color.green + g > 0xffff)
        Color.green = 0xffff;
    else if (Color.green + g < 0)
        Color.green = 0;
    else
        Color.green = (unsigned short)(fac * Color.green + g);

    /* blue */
    if (Color.blue + b > 0xffff)
        Color.blue = 0xffff;
    else if (Color.blue + b < 0)
        Color.blue = 0;
    else
        Color.blue = (unsigned short)(fac * Color.blue + b);

    Color.flags = DoRed | DoGreen | DoBlue;

    if (!XAllocColor(da_display, DefaultColormap(da_display, DefaultScreen(da_display)), &Color)) {
        fprintf(stderr, "can't allocate color %s. Using black\n", color_name);

        return BlackPixel(da_display, DefaultScreen(da_display));
    }

    return Color.pixel;
}

unsigned long da_mix_color(char *colorname1, int prop1, char *colorname2, int prop2) {
    XColor color, color1, color2;
    
    XWindowAttributes winattr;
    
    XGetWindowAttributes(da_display, da_root_win, &winattr);
    
    XParseColor(da_display, winattr.colormap, colorname1, &color1);
    XParseColor(da_display, winattr.colormap, colorname2, &color2);
    
    color.pixel = 0;
    color.red =   (color1.red   * prop1 + color2.red   * prop2) / (prop1 + prop2);
    color.green = (color1.green * prop1 + color2.green * prop2) / (prop1 + prop2);
    color.blue =  (color1.blue  * prop1 + color2.blue  * prop2) / (prop1 + prop2);
    color.flags = DoRed | DoGreen | DoBlue;
    
    XAllocColor(da_display, winattr.colormap, &color);
    /* In hex: printf("#%06lx\n", color.pixel);*/
    return color.pixel;
}

static int da_flush_expose(Window w) {
    XEvent dummy;

    int i = 0;

    while (XCheckTypedWindowEvent(da_display, w, Expose, &dummy))
        i++;

    return i;
}

void da_redraw_window(void) {
    da_redraw_window_area(0, 0, 64, 64, 0, 0);
}

void da_redraw_window_xy(int x, int y) {
    da_redraw_window_area(x, y, 64, 64, 0, 0);
}

void da_redraw_window_area(int x, int y, int sx, int sy, int dx, int dy) {
    if (da_windowed) {
        da_flush_expose(da_active_win);
        
        XCopyArea(da_display, pm_disp, da_active_win, NormalGC, x * da_scale, y * da_scale, sx * da_scale, sy * da_scale, dx * da_scale, dy * da_scale);
    
    } else {
        da_flush_expose(da_icon_win);

        XCopyArea(da_display, wmgen.pixmap, da_icon_win, NormalGC, x * da_scale, y * da_scale, sx * da_scale, sy * da_scale, dx * da_scale, dy * da_scale);

        da_flush_expose(da_win);

        XCopyArea(da_display, wmgen.pixmap, da_win, NormalGC, x * da_scale, y * da_scale, sx * da_scale, sy * da_scale, dx * da_scale, dy * da_scale);
    }
}

void da_fill_xpm_rectangle(Pixmap src, int x, int y, int dx, int dy) {
   XFillRectangle(da_display, src, NormalGC, x * da_scale, y * da_scale, dx * da_scale, dy * da_scale);
}

void da_fill_rectangle(int x, int y, int dx, int dy) {
    if (da_windowed) {
        da_fill_xpm_rectangle(pm_disp, x, y, dx, dy);
    } else {
        da_fill_xpm_rectangle(wmgen.pixmap, x, y, dx, dy);
    }
}

void da_draw_xpm_rectangle(Pixmap src, int x, int y, int dx, int dy) {
   XDrawRectangle(da_display, src, NormalGC, x * da_scale, y * da_scale, dx * da_scale, dy * da_scale);
}

void da_draw_rectangle(int x, int y, int dx, int dy) {
    if (da_windowed) {
        da_draw_xpm_rectangle(pm_disp, x, y, dx, dy);
    } else {
        da_draw_xpm_rectangle(wmgen.pixmap, x, y, dx, dy);
    }
}

void da_set_foreground(int pos) {
    XSetForeground(da_display, NormalGC, da_colors[pos].pixel);
}

/*
 * This is a hack to make some dockapps 
 * work in the windowed mode 
 */
void da_copy_wm_area(int x, int y, int sx, int sy, int dx, int dy) {
    if (da_windowed) {
        da_copy_from_xpm_to_xpm(pm_disp, pm_disp, x, y, sx, sy, dx, dy);
    }
}

void da_copy_xpm_area(int x, int y, int sx, int sy, int dx, int dy) {
    if (da_windowed) {
        da_copy_from_xpm_to_xpm(pm_main, pm_disp, x, y, sx, sy, dx, dy);
    } else {
        da_copy_from_xpm_to_xpm(wmgen.pixmap, wmgen.pixmap, x, y, sx, sy, dx, dy);
    }
}

void da_copy_from_xpm(Pixmap psource, int x, int y, int sx, int sy, int dx, int dy) {
    if (da_windowed) {
        da_copy_from_xpm_to_xpm(psource, pm_disp, x, y, sx, sy, dx, dy);
    } else {
        da_copy_from_xpm_to_xpm(psource, wmgen.pixmap, x, y, sx, sy, dx, dy);
    }
}

void da_copy_from_xpm_to_xpm(Pixmap src, Pixmap dst, int x, int y, int sx, int sy, int dx, int dy) {
    XCopyArea(da_display, src, dst, NormalGC, x * da_scale, y * da_scale, sx * da_scale, sy * da_scale, dx * da_scale, dy * da_scale);
}

void da_copy_xpm_area_with_trans(int x, int y, int sx, int sy, int dx, int dy) {
    if (da_windowed) {
        da_copy_xpm_to_xpm_with_trans(pm_main, pm_disp, x, y, sx, sy, dx, dy);
    } else {
        da_copy_xpm_to_xpm_with_trans(wmgen.pixmap, wmgen.pixmap, x, y, sx, sy, dx, dy);
    }
}

void da_copy_from_xpm_with_trans(Pixmap psource, int x, int y, int sx, int sy, int dx, int dy) {
    if (da_windowed) {
        da_copy_xpm_to_xpm_with_trans(psource, pm_disp, x, y, sx, sy, dx, dy);
    } else {
        da_copy_xpm_to_xpm_with_trans(psource, wmgen.pixmap, x, y, sx, sy, dx, dy);
    }
}

void da_copy_xpm_to_xpm_with_trans(Pixmap src, Pixmap dst, int sx, int sy, int w, int h, int dx, int dy) {
    XImage *mask;

    int x, y;

    mask = XGetImage(da_display, src, sx * da_scale, sy * da_scale, w * da_scale, h * da_scale, AllPlanes, XYPixmap);

    for (x = 0; x < w * da_scale; x++) {
        for (y = 0; y < h * da_scale; y++) {
            if (XGetPixel(mask, x, y)) {
                XCopyArea(da_display, src, dst, NormalGC, (sx * da_scale) + x, (sy * da_scale) + y, 1, 1, (dx * da_scale) + x, (dy * da_scale) + y);
            }
        }
    }

    XDestroyImage(mask);
}

/*void da_copy_xbm_area(int x, int y, int sx, int sy, int dx, int dy) {
    if (da_windowed) {
        XCopyArea(da_display, pixmask, pm_disp, NormalGC, x * da_scale, y * da_scale, sx * da_scale, sy * da_scale, dx * da_scale, dy * da_scale);
    } else {
        XCopyArea(da_display, wmgen.mask, wmgen.pixmap, NormalGC, x * da_scale, y * da_scale, sx * da_scale, sy * da_scale, dx * da_scale, dy * da_scale);
    }
}*/

void da_init_xwindow() {
    if (!(da_display = XOpenDisplay(da_display_name))) {
        fprintf(stderr, "%s: can't open display %s; using default\n", da_name, XDisplayName(da_display_name));

        if (!(da_display = XOpenDisplay(""))) {
            fprintf(stderr, "%s: can't open default display\n", da_name);

            exit(1);
        }
    }

    da_screen = DefaultScreen(da_display);

    if (da_windowed) {
        da_root_win = DefaultRootWindow(da_display);
    } else {
        da_root_win = RootWindow(da_display, da_screen);
    }

    da_display_depth = DefaultDepth(da_display, da_screen);

    da_x_fd = XConnectionNumber(da_display);

    /* reset colors: */
    for (int v = 0; v < DA_MAX_COLORS; v++) {
        if (da_colors[v].name) {
            free(da_colors[v].name);

            da_colors[v].name = NULL;

            da_colors[v].pixel = 0;
        }
    }
}

void da_open_xwindow(int argc, char *argv[], char *pixmap_bytes[], unsigned char *pixmask_bits, int pixmask_width, int pixmask_height) {
    if (da_windowed) {
        da_open_normal_win(argc, argv, pixmap_bytes, pixmask_bits, pixmask_width, pixmask_height);
    } else {
        da_open_withdr_win(argc, argv, pixmap_bytes, pixmask_bits, pixmask_width, pixmask_height);
    }
}

static void da_open_normal_win(int argc, char *argv[], char *pixmap_bytes[], unsigned char *pixmask_bits, int pixmask_width, int pixmask_height) {
    XGCValues gcv;
    unsigned long gcm;
    
    XWMHints wmhints;
    XSizeHints shints;
    XClassHint classHint;

    XpmAttributes xpmattr;
    
    _XA_GNUSTEP_WM_FUNC = XInternAtom(da_display, "_GNUSTEP_WM_FUNCTION", false);
    deleteWin = XInternAtom(da_display, "WM_DELETE_WINDOW", false);
    winType = XInternAtom(da_display, "_NET_WM_WINDOW_TYPE", false);
    dockWin = XInternAtom(da_display, "_NET_WM_WINDOW_TYPE_DOCK", false);

    shints.x = 0;
    shints.y = 0;
    shints.flags = USSize | PMinSize | PMaxSize | PBaseSize | USPosition;
    
    XWMGeometry(da_display, da_screen, da_geometry, NULL, 0, &shints, &shints.x, &shints.y, &shints.width, &shints.height, &shints.win_gravity);

    shints.width = 64 * da_scale;
    shints.height = 64 * da_scale;
    shints.min_width = shints.width;
    shints.min_height = shints.height;
    shints.max_width = shints.width;
    shints.max_height = shints.height;
    shints.base_width = shints.width;
    shints.base_height = shints.height;
    
    da_main_win = XCreateSimpleWindow(da_display, da_root_win, shints.x, shints.y, 64 * da_scale, 64 * da_scale, 0, 0, 0);

    if (da_dock_win) {
        XChangeProperty(da_display, da_main_win, winType, XA_ATOM, 32, PropModeReplace, (unsigned char *)&dockWin, 1);
    }
    
    classHint.res_name = da_name;
    classHint.res_class = da_name;
    
    XSetClassHint(da_display, da_main_win, &classHint);
   
    wmhints.initial_state = NormalState;
    wmhints.flags = WindowGroupHint | StateHint;
    da_active_win = da_main_win;
    
    wmhints.window_group = da_main_win;
    XSetWMHints(da_display, da_main_win, &wmhints);
    XSetWMNormalHints(da_display, da_main_win, &shints);
    XSetCommand(da_display, da_main_win, argv, argc);
    XStoreName(da_display, da_main_win, da_name);
    XSetIconName(da_display, da_main_win, da_name);
    XSetWMProtocols(da_display, da_active_win, &deleteWin, 1);

    gcm = GCGraphicsExposures;
    gcv.graphics_exposures = false;
    
    NormalGC = XCreateGC(da_display, da_root_win, gcm, &gcv);

    xpmattr.numsymbols = DA_MAX_COLORS;
    xpmattr.colorsymbols = da_colors;
    xpmattr.valuemask = XpmColorSymbols;

    XpmCreatePixmapFromData(da_display, da_main_win, pixmap_bytes, &pm_main, &pixmask, &xpmattr);
    XpmCreatePixmapFromData(da_display, da_root_win, pixmap_bytes, &pm_disp, NULL, &xpmattr);
    /*pm_disp = XCreatePixmap(da_display, da_root_win, pixmask_width, pixmask_height, da_display_depth);*/

    if (pixmask_bits) {
        pixmask = XCreateBitmapFromData(da_display, da_main_win, (char *)pixmask_bits, pixmask_width, pixmask_height);
    }
    
    if (da_back_image) {
        if (!XpmReadFileToPixmap(da_display, da_root_win, da_back_image, &pm_tile, &pixmask_tile, &xpmattr)) {
            XCopyArea(da_display, pm_tile, pm_disp, NormalGC, 0, 0, 64 * da_scale, 64 * da_scale, 0, 0);
            
            XShapeCombineMask(da_display, da_active_win, ShapeBounding, 0, 0, pixmask_tile, ShapeSet);
        
        } else {
            printf("WARN: failed to set the \"%s\" background image!\n", da_back_image);
            da_log(WARN, "failed to set the \"%s\" background image!\n", da_back_image);
            
            XShapeCombineMask(da_display, da_active_win, ShapeBounding, 0, 0, pixmask, ShapeSet);
        }
        
    } else {
        XShapeCombineMask(da_display, da_active_win, ShapeBounding, 0, 0, pixmask, ShapeSet);
    }
    
    XSetClipMask(da_display, NormalGC, pixmask);
    XCopyArea(da_display, pm_main, pm_disp, NormalGC, 0, 0, pixmask_width, pixmask_height, 0, 0);
    XSetClipMask(da_display, NormalGC, None);

    XSelectInput(da_display, da_active_win, ExposureMask | ButtonPressMask | ButtonReleaseMask | ButtonMotionMask | PointerMotionMask | StructureNotifyMask | EnterWindowMask | LeaveWindowMask | KeyPressMask | KeyReleaseMask);
    XMapWindow(da_display, da_main_win);
}

static void da_open_withdr_win(int argc, char *argv[], char *pixmap_bytes[], unsigned char *pixmask_bits, int pixmask_width, int pixmask_height) {    
    unsigned int borderwidth = 1;

    XClassHint classHint;

    XTextProperty name;

    XGCValues gcv;

    unsigned long gcm;

    int dummy = 0;

    wmgen.attributes.valuemask |= XpmColorSymbols;
    wmgen.attributes.numsymbols = DA_MAX_COLORS;
    wmgen.attributes.colorsymbols = da_colors;
    
    if (XpmCreatePixmapFromData(da_display, da_root_win, pixmap_bytes, &(wmgen.pixmap), &(wmgen.mask), &(wmgen.attributes)) != XpmSuccess) {
        fprintf(stderr, "Not enough free colorcells.\n");

        exit(1);
    }
    
    /*
     *  Create a window
     */
    mysizehints.flags = USSize | USPosition;
    mysizehints.x = 0;
    mysizehints.y = 0;

    back_pix = da_get_color("white");
    fore_pix = da_get_color("black");

    XWMGeometry(da_display, da_screen, da_geometry, NULL, borderwidth, &mysizehints, &mysizehints.x, &mysizehints.y,&mysizehints.width,&mysizehints.height, &dummy);

    /* additional checks if a mask is not square: */
    /*if (pixmask_width > pixmask_height) {
        mysizehints.width = pixmask_height;
    } else {
        mysizehints.width = pixmask_width;
    }
    if (pixmask_height > pixmask_width) {
        mysizehints.height = pixmask_width;
    } else {
        mysizehints.height = pixmask_height;
    }*/

    da_win = XCreateSimpleWindow(da_display, da_root_win, mysizehints.x, mysizehints.y, 64 * da_scale, 64 * da_scale, borderwidth, fore_pix, back_pix);

    da_icon_win = XCreateSimpleWindow(da_display, da_win, mysizehints.x, mysizehints.y, 64 * da_scale, 64 * da_scale, borderwidth, fore_pix, back_pix);

    /*
     *  Activate hints
     */
    XSetWMNormalHints(da_display, da_win, &mysizehints);

    classHint.res_name = da_name;
    classHint.res_class = da_name;

    XSetClassHint(da_display, da_win, &classHint);

    /*
     *  Set up the xevents that you want the relevent windows to inherit
     *  Currently, its seems that setting KeyPress events here has no
     *  effect. I.e. for some you will need to Grab the focus and then return
     *  it after you are done...
     */
    XSelectInput(da_display, da_win, ButtonPressMask | ExposureMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask | EnterWindowMask | LeaveWindowMask | KeyPressMask | KeyReleaseMask);
    XSelectInput(da_display, da_icon_win, ButtonPressMask | ExposureMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask | EnterWindowMask | LeaveWindowMask | KeyPressMask | KeyReleaseMask);

    if (XStringListToTextProperty(&da_name, 1, &name) == 0) {
        fprintf(stderr, "%s: can't allocate window name\n", da_name);

        exit(1);
    }

    XSetWMName(da_display, da_win, &name);

    /*
     *   Create Graphics Context (GC) for drawing
     */
    gcm = GCForeground | GCBackground | GCGraphicsExposures;

    gcv.foreground = fore_pix;
    gcv.background = back_pix;
    gcv.graphics_exposures = 0;

    NormalGC = XCreateGC(da_display, da_root_win, gcm, &gcv);

    if (pixmask_bits) {
        pixmask = XCreateBitmapFromData(da_display, da_win, (char *)pixmask_bits, pixmask_width, pixmask_height);

        XShapeCombineMask(da_display, da_win, ShapeBounding, 0, 0, pixmask, ShapeSet);
        XShapeCombineMask(da_display, da_icon_win, ShapeBounding, 0, 0, pixmask, ShapeSet);
    } else {
        XShapeCombineMask(da_display, da_win, ShapeBounding, 0, 0, wmgen.mask, ShapeSet);
        XShapeCombineMask(da_display, da_icon_win, ShapeBounding, 0, 0, wmgen.mask, ShapeSet);
    }

    mywmhints.initial_state = WithdrawnState;
    mywmhints.flags = StateHint | IconWindowHint | IconPositionHint | WindowGroupHint;
    mywmhints.icon_window = da_icon_win;
    mywmhints.icon_x = mysizehints.x;
    mywmhints.icon_y = mysizehints.y;
    mywmhints.window_group = da_win;

    XSetWMHints(da_display, da_win, &mywmhints);

    XSetCommand(da_display, da_win, argv, argc);

    XMapWindow(da_display, da_win);
}

void da_add_mouse_region(int index, int left, int top, int right, int bottom) {
    if (index < DA_MAX_MOUSE_REGION) {
        mouse_region[index].enable = true;
        mouse_region[index].top = top * da_scale;
        mouse_region[index].left = left * da_scale;
        mouse_region[index].bottom = bottom * da_scale;
        mouse_region[index].right = right * da_scale;
    }
}

void da_enable_mouse_region(int index) {
    if (index < DA_MAX_MOUSE_REGION) {
        mouse_region[index].enable = true;
    }
}

void da_disable_mouse_region(int index) {
    if (index < DA_MAX_MOUSE_REGION) {
        mouse_region[index].enable = false;
    }
}

int da_check_mouse_region() {
    int i;
    int found;
    int x, y;
    
    x = da_xevent.xbutton.x;
    y = da_xevent.xbutton.y;

    found = 0;

    for (i = 0; i < DA_MAX_MOUSE_REGION && !found; i++) {
        if (mouse_region[i].enable &&
            x <= mouse_region[i].right &&
            x >= mouse_region[i].left &&
            y <= mouse_region[i].bottom &&
            y >= mouse_region[i].top)
                found = 1;
    }
    
    if (!found)
        return -1;

    return (i - 1);
}

void da_set_mask_xy(int sx, int sy) {
    XImage *mask, *mask_tile;

    int x, y;

    if (da_windowed) {
        if (pm_tile) {
            mask = XGetImage(da_display, pixmask, sx * da_scale, sy * da_scale, 64 * da_scale, 64 * da_scale, AllPlanes, XYPixmap);
            mask_tile = XGetImage(da_display, pixmask_tile, 0, 0, 64 * da_scale, 64 * da_scale, AllPlanes, XYPixmap);

            for (x = 0; x < 64 * da_scale; x++) {
                for (y = 0; y < 64 * da_scale; y++) {
                    /*if (!XGetPixel(mask_tile, x, y)) {
                        continue;
                    }*/
                    
                    if (!XGetPixel(mask, x, y)) {
                        XCopyArea(da_display, pm_tile, pm_disp, NormalGC, x, y, 1, 1, x, y);
                    }
                }
            }

            XDestroyImage(mask);
            XDestroyImage(mask_tile);
        
        } else {
            XShapeCombineMask(da_display, da_active_win, ShapeBounding, -sx * da_scale, -sy * da_scale, pixmask, ShapeSet);
        }
    
    } else {
        if (pixmask) {
            XShapeCombineMask(da_display, da_win, ShapeBounding, -sx * da_scale, -sy * da_scale, pixmask, ShapeSet);
            XShapeCombineMask(da_display, da_icon_win, ShapeBounding, -sx * da_scale, -sy * da_scale, pixmask, ShapeSet);
        } else {
            XShapeCombineMask(da_display, da_win, ShapeBounding, -sx * da_scale, -sy * da_scale, wmgen.mask, ShapeSet);
            XShapeCombineMask(da_display, da_icon_win, ShapeBounding, -sx * da_scale, -sy * da_scale, wmgen.mask, ShapeSet);
        }
    }
}

void da_set_mask(Pixmap pm, int sx, int sy, int w, int h, int dx, int dy) {
    XImage *mask, *mask_tile;

    int x, y;

    if (da_windowed) {
        if (pm_tile) {
            mask = XGetImage(da_display, pm, sx * da_scale, sy * da_scale, w * da_scale, h * da_scale, AllPlanes, XYPixmap);
            mask_tile = XGetImage(da_display, pixmask_tile, 0, 0, 64 * da_scale, 64 * da_scale, AllPlanes, XYPixmap);

            for (x = 0; x < w * da_scale; x++) {
                for (y = 0; y < h * da_scale; y++) {
                    /*if (!XGetPixel(mask_tile, x, y)) {
                        continue;
                    }*/
                    
                    if (!XGetPixel(mask, x, y)) {
                        XCopyArea(da_display, pm_tile, pm_disp, NormalGC, x, y, 1, 1, x + (dx * da_scale), y + (dy * da_scale));
                    }
                }
            }

            XDestroyImage(mask);
            XDestroyImage(mask_tile);
        
        } else {
            XShapeCombineMask(da_display, da_active_win, ShapeBounding, -sx * da_scale, -sy * da_scale, pm, ShapeSet);
        }
    
    } else {
        XShapeCombineMask(da_display, da_win, ShapeBounding, -sx * da_scale, -sy * da_scale, pm, ShapeSet);
        XShapeCombineMask(da_display, da_icon_win, ShapeBounding, -sx * da_scale, -sy * da_scale, pm, ShapeSet);
    }
}

int da_watch_xevent() {
    while (XPending(da_display)) {
        XNextEvent(da_display, &da_xevent);
            
        switch (da_xevent.type) {
        case Expose:
            da_redraw_window();

            return EXPOSE;

            break;
                
        case DestroyNotify:
            XCloseDisplay(da_display);
            
            exit(0);
            
            return EXIT;

            break;

        case ClientMessage:
            if (da_windowed && da_xevent.xclient.data.l[0] == (int)deleteWin) {
                exit(0);
            
                return EXIT;
            }

            break;

        case ButtonPress:
            switch (da_xevent.xbutton.button) {
            case Button1:
                return MOUSE_1_PRS;

                break;

            case Button2:
                return MOUSE_2_PRS;

                break;
                
            case Button3:
                return MOUSE_3_PRS;

                break;

            case Button4:
                return MOUSE_4;

                break;

            case Button5:
                return MOUSE_5;

                break;

            default:
                break;
            }

            break;
            
        case ButtonRelease:
            switch (da_xevent.xbutton.button) {
            case Button1:
                return MOUSE_1_REL;

                break;

            case Button2:
                return MOUSE_2_REL;

                break;
                
            case Button3:
                return MOUSE_3_REL;

                break;

            case Button4:
                return MOUSE_4;

                break;

            case Button5:
                return MOUSE_5;

                break;

            default:
                break;
            }

            break;

        default:
            break;
        }
    }

    return -1;
}

/* WIP */
/*void da_create_bitmap_data_from_pixmap(Pixmap pm) {
    int x, y, w, h;

    static unsigned char bitmap_data[(64 * da_scale) * (64 * da_scale)];
    
    unsigned char elem;

    XImage *mask;

    Pixmap test;

    mask = XGetImage(da_display, pm, 0, 0, 64 * da_scale, 64 * da_scale, AllPlanes, XYPixmap);

    for (x = 0; x < 64 * da_scale; x++) {
        for (y = 0; y < 64 * da_scale; y++) {
            if (!XGetPixel(mask, x, y)) {
                // white
            } else {
                // black
            }
        }
    }

    test = XCreateBitmapFromData(da_display, da_root_win, (char *)bitmap_data, 64 * da_scale, 64 * da_scale);

    XWriteBitmapFile(da_display, "test.xbm", test, 64 * da_scale, 64 * da_scale, 0, 0);

    XDestroyImage(mask);
}*/

/* Taken from wmweather */
void da_create_xbm_from_xpm(char *xbm, char **xpm, int sx, int sy) {
    int i, j;
    int width, height, numcol, chars;
    int bcount;
    
    char *zero, *color, *pos;
    
    unsigned char bwrite;

    sscanf(*xpm, "%d %d %d %d", &width, &height, &numcol, &chars);

    zero  = malloc(chars * sizeof(char));
    color = malloc(chars * sizeof(char));
    
    memcpy(zero, xpm[1], chars);
    
    for (i = numcol + 1; i < numcol + sy + 1; i++) {
        bcount = 0;
        bwrite = 0;
        
        pos = xpm[i];
        
        for (j = 0; j < sx; j++) {
            bwrite >>= 1;
            
            memcpy(color, pos, chars);
            
            if (memcmp(color, zero, chars)) {
                bwrite += 128;
            }
            
            bcount++;
            
            if (bcount == 8) {
                *xbm = bwrite;
                
                xbm++;
                
                bcount = 0;
                bwrite = 0;
            }
            
            pos += chars * sizeof(char);
        }
    }
    
    free(zero);
    free(color);
}

void da_draw_string(int x_pos, int y_pos, char *font, char *string) {
    XFontStruct *fontStruct;

    if ((fontStruct = XLoadQueryFont(da_display, font)) == 0) {
        printf("ERR: could not load font \"%s\"\n", font);
        da_log(ERR, "could not load font \"%s\"\n", font);
        exit(0);
    }

    int strLength = strlen(string);
    int strWidth = XTextWidth(fontStruct, string, strLength);

    if (x_pos == CENTER_TEXT) {
        x_pos = ((64 * da_scale) / 2) - (strWidth / 2);
    } else {
        x_pos *= da_scale;
    }

    XSetFont(da_display, NormalGC, fontStruct->fid);

    XDrawString(da_display, da_windowed ? pm_disp : wmgen.pixmap, NormalGC, x_pos, y_pos * da_scale, string, strLength);

    XFreeFont(da_display, fontStruct);
}

/*void da_read_file_to_pixmap(char *file_path, Pixmap pm, Pixmap pm_mask) {
    XpmReadFileToPixmap(da_display, da_root_win, file_path, &pm, &pm_mask, &xpmattr);
}*/

/* WIP */
char **da_scale_image(char *src_data[], int w, int h) {
    int x, y;
    int dx = 0, dy = 0;
    int counter;

    static char **dst_data;
    
    Pixmap src_px;
    Pixmap dst_px;

    XImage *src_img;
    XImage *dst_img;
    
    XpmCreatePixmapFromData(da_display, da_root_win, src_data, &src_px, NULL, NULL);

    src_img = XGetImage(da_display, src_px, 0, 0, w, h, AllPlanes, ZPixmap);
    
    dst_px = XCreatePixmap(da_display, da_root_win, w * da_scale, h * da_scale, da_display_depth);

    for (x = 0; x < w; x++) {
        for (y = 0; y < h; y++) {
            if (XGetPixel(src_img, x, y)) {
                for (counter = 0; counter < da_scale; counter++) {
                    dx = x + counter;
                    dy = y + counter;
                    
                    XCopyArea(da_display, src_px, dst_px, NormalGC, x, y, 1, 1, dx, dy);
                }
            }
        }
    }

    dst_img = XGetImage(da_display, dst_px, 0, 0, w * da_scale, h * da_scale, AllPlanes, ZPixmap);
    
    XpmCreateDataFromImage(da_display, &dst_data, dst_img, NULL, NULL);

    XFreePixmap(da_display, src_px);
    XFreePixmap(da_display, dst_px);
    XDestroyImage(src_img);
    XDestroyImage(dst_img);

    return dst_data;
}

/*static int get_shift(unsigned mask) {
    int i = 0;

    while (!mask & 1) {
        mask >>= 1;
        i++;
    }
    
    return i;
}

void da_combine_with_trans(int sx, int sy, int w, int h, int dx, int dy) {
    if (da_windowed) {
        da_combine_xpm_with_trans(pm_disp, pixmask, sx, sy, w, h, dx, dy);
    } else {
        da_combine_xpm_with_trans(wmgen.pixmap, wmgen.mask, sx, sy, w, h, dx, dy);
    }
}

void da_combine_xpm_with_trans(Pixmap p, Pixmap m, int sx, int sy, int w, int h, int dx, int dy) {
    XImage *pix, *mask;

    Window baz;
    
    unsigned int ww, hh, bar;
    
    int foo;
    int x, y;

    XGetGeometry(da_display, p, &baz, &foo, &foo, &ww, &hh, &bar, &bar);

    pix = XGetImage(da_display, p, 0, 0, ww, hh, AllPlanes, ZPixmap);

    XGetGeometry(da_display, m, &baz, &foo, &foo, &ww, &hh, &bar, &bar);

    mask = XGetImage(da_display, m, 0, 0, ww, hh, AllPlanes, ZPixmap);

    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++) {
            if (!XGetPixel(mask, (sx+x) * da_scale, (sy+y) * da_scale))
                continue;
                
            XPutPixel(pix, (dx+x) * da_scale, (dy+y) * da_scale, XGetPixel(pix, (sx+x) * da_scale, (sy+y) * da_scale));
        }
    }

    XPutImage(da_display, p, NormalGC, pix, 0, 0, 0, 0, pix->width * da_scale, pix->height * da_scale);

    XDestroyImage(pix);
    XDestroyImage(mask);
}

void combineWithOpacity(int sx, int sy, unsigned w, unsigned h, int dx, int dy, int o){
    XImage *pix, *mask;
    unsigned int ww, hh, bar;
    int foo;
    Window baz;
    int rmask, gmask, bmask;
    int rshift, gshift, bshift;
    unsigned long spixel, dpixel;
    unsigned x, y;
    int c_o;

    if(o==0) return;
    if(o==256){
        combineWithTrans(sx, sy, w, h, dx, dy);
        return;
    }

    XGetGeometry(display, wmgen.pixmap, &baz, &foo, &foo, &ww, &hh, &bar, &bar);
    pix=XGetImage(display, wmgen.pixmap, 0, 0, ww, hh, AllPlanes, ZPixmap);
    XGetGeometry(display, wmgen.mask, &baz, &foo, &foo, &ww, &hh, &bar, &bar);
    mask=XGetImage(display, wmgen.mask, 0, 0, ww, hh, AllPlanes, ZPixmap);

    if (pix->depth == DefaultDepth(display, screen)) {{
        Visual *visual=DefaultVisual(display, screen);
        rmask = visual->red_mask;
        gmask = visual->green_mask;
        bmask = visual->blue_mask;
    }} else {
        rmask = pix->red_mask;
        gmask = pix->green_mask;
        bmask = pix->blue_mask;
    }

    c_o=256-o;
    rshift=get_shift(rmask);
    gshift=get_shift(gmask);
    bshift=get_shift(bmask);
// NOTE: >>s then <<s to prevent overflow when multiplying opacity
#define AVG(m, s) ((((((spixel&m)>>s)*o+((dpixel&m)>>s)*c_o)>>8)<<s)&m)
    for(y=0; y<h; y++){
        for(x=0; x<w; x++){
            if(!XGetPixel(mask, (sx+x) * scale, (sy+y) * scale)) continue;
            spixel=XGetPixel(pix, (sx+x) * scale, (sy+y) * scale);
            if(!XGetPixel(mask, (dx+x) * scale, (dy+y) * scale)){
                XPutPixel(pix, (dx+x) * scale, (dy+y) * scale, spixel);
            } else {
                dpixel=XGetPixel(pix, (dx+x) * scale, (dy+y) * scale);
                XPutPixel(pix, (dx+x) * scale, (dy+y) * scale,
                          AVG(rmask, rshift) |
                          AVG(gmask, gshift) |
                          AVG(bmask, bshift));
            }
        }
    }
#undef AVG
    XPutImage(display, wmgen.pixmap, NormalGC, pix, 0, 0, 0, 0, pix->width * scale, pix->height * scale);

    XDestroyImage(pix);
    XDestroyImage(mask);
}*/
