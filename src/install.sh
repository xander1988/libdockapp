#!/bin/bash

if [ ! -d /usr/local/include/libdockapp4 ]; then
    mkdir /usr/local/include/libdockapp4;
fi

cp daconf.h /usr/local/include/libdockapp4/daconf.h;
cp dockapp.h /usr/local/include/libdockapp4/dockapp.h;
cp daxutils.h /usr/local/include/libdockapp4/daxutils.h;
cp damisc.h /usr/local/include/libdockapp4/damisc.h;
cp libdockapp4.so /usr/local/lib/libdockapp4.so;
