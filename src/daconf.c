/* (C) 2021 Xander
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file COPYING); if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307, USA
 */

#include "daconf.h"

int total_options = -1;    /* counter of initialized options */

static char *set_spaces(const int, int);
static char *trim_string(char *);

static char *trim_string(char *string) {
    int first_quote = -1;
    int ending_quote = -1;
    int length, i, ii = 0;

    static char *string_tmp;
    
    bool it_is_in_quotes = false;
    bool there_is_a_space = false;

    length = strlen(string);

    for (i = 0; i < length; i++) {
        if (string[i] == '"') {
            if (first_quote >= 0 && ending_quote == -1) {
                ending_quote = i;
            }

            if (first_quote == -1) {
                first_quote = i + 1;
            }
            
            it_is_in_quotes = true;
        }

        if (string[i] == ' ') {
            there_is_a_space = true;
        }
    }

    /* Get rid of double quotes */
    if (it_is_in_quotes) {
        string_tmp = malloc(length + 1);
        memset(string_tmp, '\0', length);

        for (i = 0; first_quote < ending_quote; i++) {
            string_tmp[i] = string[first_quote];

            first_quote++;
        }

        strcpy(string, string_tmp);
    }

    /* No double quotes found -- just get rid of spaces */
    if (!it_is_in_quotes && there_is_a_space) {
        string_tmp = malloc(length + 1);
        memset(string_tmp, '\0', length);

        for (i = 0; i < length; i++) {
            if (string[i] == ' ') {
                i++;
            }

            string_tmp[ii] = string[i];

            ii++;
        }

        strcpy(string, string_tmp);
    }

    if (string_tmp) {
        free(string_tmp);

        string_tmp = NULL;
    }

    return string;
}

static char *set_spaces(const int max, int count) {
    static char *spaces;

    spaces = malloc(max);
    
    memset(spaces, ' ', max);
    
    if (count > max - 1) {
        spaces[max - 1] = 0;
    } else {
        spaces[max - count] = 0;
    }

    return spaces;
}

void da_print_version() {
    printf("%s %s\n", da_name, da_version);
}

void da_print_usage() {
    int k;

    bool conf_initialized = false;
    
    printf("%s %s - %s\n\n", da_name, da_version, da_descr);
    printf("%s uses the following configuration files (priority ascending):\n\n", da_name);

    for (k = 0; k < DA_MAX_CONF_FILES; k++) {
        if (da_config[k].path) {
            conf_initialized = true;
            
            printf("%s\n", da_config[k].path);
        }
    }

    /* if no conf files set: */
    if (!conf_initialized) {
        printf("none.\n");
    }

    printf("\nOptions (\"none\" means it's a string and no default is set, \"no\" and \"yes\" are boolean, the rest are integers and doubles; arguments containing spaces must be enclosed in double quotes):\n\n");
    
    for (k = 0; k <= total_options; k++) {
        char *blank;

        int count = 0;

        count = printf("%s ", da_option[k].name);

        blank = set_spaces(20, count);

        if (da_option[k].key) {
            count += printf("%s %s %s", blank, da_option[k].key, da_option[k].long_key);
        } else {
            count += printf("%s    %s", blank, da_option[k].long_key);
        }

        /*blank = set_spaces(5, count);*/
        
        switch (da_option[k].type) {
        case STRING:
            count += printf("%sdef: %s", blank, da_option[k].def_str_val ? da_option[k].def_str_val : "none");
            break;

        case INTEGER:
            if ((da_option[k].min_alld_int != da_option[k].max_alld_int) != 0) {
                count += printf("%sfrom %i to %i, def: %i", blank, da_option[k].min_alld_int, da_option[k].max_alld_int, da_option[k].def_int_val);
            } else {
                count += printf("%sdef: %i", blank, da_option[k].def_int_val);
            }
                
            break;

        case FLOATING:
            if ((da_option[k].min_alld_flt != da_option[k].max_alld_flt) != 0.0) {
                count += printf("%sfrom %lf to %lf, def: %lf", blank, da_option[k].min_alld_flt, da_option[k].max_alld_flt, da_option[k].def_flt_val);
            } else {
                count += printf("%sdef: %lf", blank, da_option[k].def_flt_val);
            }
            
            break;

        case BOOLEAN:
            count += printf("%sdef: %s", blank, da_option[k].def_bool_val == true ? "yes" : "no");
            
            break;
        
        default:
            break;
        }

        blank = set_spaces(75, count);
        
        printf("%s %s.\n", blank, da_option[k].descr);

        if (blank) {
            free(blank);

            blank = NULL;
        }
    }
}

void da_parse_all(int argc, char *argv[]) {
    da_parse_conf_files();

    da_parse_args(argc, argv);

    if (da_showhelp) {
        da_print_usage();
            
        exit(0);
    }

    if (da_showver) {
        da_print_version();
            
        exit(0);
    }
}

void da_parse_args(int argc, char *argv[]) {
    for (int i = 1; i < argc; i++) {
        for (int t = 0; t <= total_options; t++) {      
            if ((da_option[t].key && !strcmp(argv[i], da_option[t].key)) || !strcmp(argv[i], da_option[t].long_key)) {                
                if (da_option[t].type == BOOLEAN) {
                    da_insert_value(t, NULL);
                    
                } else {
                    if (!argv[i + 1]) {
                        printf("WARN: option \"%s\" missing parameter, skipping...\n", argv[i]);
                        da_log(WARN, "option \"%s\" missing parameter, skipping...\n", argv[i]);
                        
                    } else {
                        da_insert_value(t, argv[i + 1]);
                
                        i++;
                    }
                }
            }
        }
    }
}

void da_init_conf_file(DALocation loc, char *config_file) {
    int number = 0;

start_over:
    if (!da_config[number].path) {
        switch (loc) {
        case SYSTEM:
            da_config[number].path = malloc(strlen(config_file) + 1);
    
            sprintf(da_config[number].path, "%s", config_file);

            break;
            
        case LOCAL:
            da_config[number].path = malloc(strlen(getenv("HOME")) + strlen("/") + strlen(config_file) + 1);
    
            sprintf(da_config[number].path, "%s/%s", getenv("HOME"), config_file);

            break;

        default:
            break;
        }
    
    } else {
        number++;

        if (number == DA_MAX_CONF_FILES) {
            printf("WARN: config file \"%s\" cannot be initialized (max files of %i reached)!\n", config_file, DA_MAX_CONF_FILES);
            da_log(WARN, "config file \"%s\" cannot be initialized (max files of %i reached)!\n", config_file, DA_MAX_CONF_FILES);

            return;
        }

        goto start_over;
    }
}

void da_parse_conf_files() {
    for (int u = 0; u < DA_MAX_CONF_FILES; u++) {
        if (da_config[u].path) {       
            if (stat(da_config[u].path, &da_filestat) == 0) {
                da_parse_file(da_config[u].path);
                
                if (da_config[u].last_mod_time != da_filestat.st_mtime) {
                    da_config[u].last_mod_time = da_filestat.st_mtime;
                }
            }
        }
    }
}

void da_parse_file(char *config_file) {
    FILE *file = NULL;

    char buffer[512];
    
    char *p      = NULL;
    char *optarg = NULL;
    
    int line;

    if ((file = fopen(config_file, "r")) == NULL) {
        printf("WARN: unable to open \"%s\"!\n", config_file);
        da_log(WARN, "unable to open \"%s\"!\n", config_file);
        
        return;
    }
    
    for (line = 1; ; line++) {
        p = fgets(buffer, sizeof(buffer), file);
        
        if (feof(file))
            break;
            
        if ((p = strchr(buffer, '\n')) != NULL)
            *p = '\0';

        if ((p = strchr(buffer, '"')) == NULL) {
            while ((p = strchr(buffer, ' ')) || (p = strchr(buffer, '\t'))) {
                do {
                    *p = *(p + 1);
                } while (*p++);
            }
        }
        
        if (*buffer == '\0' || *buffer == '#')
            continue;
            
        if ((optarg = strchr(buffer, '=')) != NULL)
            *(optarg++) = '\0';

        optarg = trim_string(optarg);
        
        strcpy(buffer, trim_string(buffer));

        for (int t = 0; t <= total_options; t++) {
            if (!strcmp(buffer, da_option[t].name) && optarg) {
                da_insert_value(t, optarg);
            }
        }
    }
    
    fclose(file);
}

void da_insert_value(int pos, char *value) {
    int intr;
    double dbl;
    
    switch (da_option[pos].type) {
    case STRING:
        if (*da_option[pos].str_val) {
            free(*da_option[pos].str_val);

            *da_option[pos].str_val = NULL;
        }
    
        *da_option[pos].str_val = malloc(strlen(value) + 1);
        strcpy(*da_option[pos].str_val, value);
            
        break;
            
    case INTEGER:
        sscanf(value, "%d", &intr);

        if ((da_option[pos].min_alld_int != da_option[pos].max_alld_int) != 0) {
            if (intr < da_option[pos].min_alld_int) {
                printf("WARN: %s is less than %d, truncating...\n", da_option[pos].name, da_option[pos].min_alld_int);
                da_log(WARN, "%s is less than %d, truncating...\n", da_option[pos].name, da_option[pos].min_alld_int);

                intr = da_option[pos].min_alld_int;
            }

            if (intr > da_option[pos].max_alld_int) {
                printf("WARN: %s is bigger than %d, truncating...\n", da_option[pos].name, da_option[pos].max_alld_int);
                da_log(WARN, "%s is bigger than %d, truncating...\n", da_option[pos].name, da_option[pos].max_alld_int);

                intr = da_option[pos].max_alld_int;
            }
        }
        
        *da_option[pos].int_val = intr;
                
        break;
            
    case FLOATING:
        sscanf(value, "%lf", &dbl);

        if ((da_option[pos].min_alld_flt != da_option[pos].max_alld_flt) != 0.0) {
            if (dbl < da_option[pos].min_alld_flt) {
                printf("WARN: %s is less than %lf, truncating...\n", da_option[pos].name, da_option[pos].min_alld_flt);
                da_log(WARN, "%s is less than %lf, truncating...\n", da_option[pos].name, da_option[pos].min_alld_flt);

                dbl = da_option[pos].min_alld_flt;
            }

            if (dbl > da_option[pos].max_alld_flt) {
                printf("WARN: %s is bigger than %lf, truncating...\n", da_option[pos].name, da_option[pos].max_alld_flt);
                da_log(WARN, "%s is bigger than %lf, truncating...\n", da_option[pos].name, da_option[pos].max_alld_flt);

                dbl = da_option[pos].max_alld_flt;
            }
        }
        
        *da_option[pos].flt_val = dbl;
            
        break;
            
    case BOOLEAN:
        if (value) {
            *da_option[pos].bool_val = da_parse_bool_option(value);
        } else {
            *da_option[pos].bool_val = *da_option[pos].bool_val ? false : true;
        }
            
        break;
    
    default:
        break;
    }
}

bool da_parse_bool_option(char *option) {
    char *vars[] = {"YES", "OK", "TRUE", "ON", "1"};
    
    char option_up[10];

    int length, c, l;

    length = strlen(option);

    for (c = 0; c < length; c++) {
        option_up[c] = toupper(option[c]);
    }

    for (l = 0; l < 5; l++) {
        if (!strcmp(vars[l], option_up)) {
            return true;
        }
    }

    return false;
}

bool da_conf_changed(int argc, char *argv[]) {
    for (int u = 0; u < DA_MAX_CONF_FILES; u++) {
        if (da_config[u].path) {
            if (stat(da_config[u].path, &da_filestat) == 0) {
                if (da_config[u].last_mod_time != da_filestat.st_mtime) {
                    da_config[u].last_mod_time = da_filestat.st_mtime;

                    da_reset_to_defaults();
        
                    da_parse_all(argc, argv);

                    XCloseDisplay(da_display);
            
                    return true;
                }
            }
        }
    }

    return false;
}

void da_reset_to_defaults() {
    for (int t = 0; t <= total_options; t++) {
        switch (da_option[t].type) {
        case STRING:
            if (*da_option[t].str_val) {
                free(*da_option[t].str_val);

                *da_option[t].str_val = NULL;
            }

            if (da_option[t].def_str_val) {
                *da_option[t].str_val = malloc(strlen(da_option[t].def_str_val) + 1);
                strcpy(*da_option[t].str_val, da_option[t].def_str_val);
            }
            
            break;
            
        case INTEGER:
            *da_option[t].int_val = da_option[t].def_int_val;
                
            break;
            
        case FLOATING:
            *da_option[t].flt_val = da_option[t].def_flt_val;
            
            break;
            
        case BOOLEAN:
            *da_option[t].bool_val = da_option[t].def_bool_val;
            
            break;

        default:
            break;
        }
    }
}

void da_init_main(char *argv_0, char *prog_version, char *prog_descr) {
    /* init dockapp name */
    da_name = malloc(strlen(argv_0) + 1);
    strcpy(da_name, argv_0);

    /* init dockapp version */
    da_version = malloc(strlen(prog_version) + 1);
    strcpy(da_version, prog_version);

    /* init dockapp description */
    da_descr = malloc(strlen(prog_descr) + 1);
    strcpy(da_descr, prog_descr);

#ifdef DA_WRITE_LOG
    /* init log file */
    da_log_file = malloc(strlen(getenv("HOME")) + strlen("/.config/dockapps/dockapps.log") + 1);
    sprintf(da_log_file, "%s/.config/dockapps", getenv("HOME"));
    
    if (chdir(da_log_file)) {
        if (errno == ENOENT) {
            if (mkdir(da_log_file, 0777)) {
                printf("%s: cannot create %s: %m\n", da_name, da_log_file);
                
                exit(errno);
            }
            
        } else {
            printf("%s: cannot chdir into %s: %m\n", da_name, da_log_file);
            
            exit(errno);
        }
    }

    strcat(da_log_file, "/dockapps.log");
#endif
}

void da_init_opt_info(char *opt_name, char *opt_key, char *opt_longk, char *description) {
    total_options++;

    if (total_options == DA_MAX_OPTIONS) {
        printf("ERR: option \"%s\" cannot be initialized (max options of %i reached)!\n", opt_name, DA_MAX_OPTIONS);
        da_log(ERR, "option \"%s\" cannot be initialized (max options of %i reached)!\n", opt_name, DA_MAX_OPTIONS);

        exit(1);
    }
    
    da_option[total_options].name = malloc(strlen(opt_name) + 1);
    strcpy(da_option[total_options].name, opt_name);

    if (opt_key) {
        da_option[total_options].key = malloc(strlen(opt_key) + 1);
        strcpy(da_option[total_options].key, opt_key);
    }
    
    da_option[total_options].long_key = malloc(strlen(opt_longk) + 1);
    strcpy(da_option[total_options].long_key, opt_longk);
    
    da_option[total_options].descr = malloc(strlen(description) + 1);
    strcpy(da_option[total_options].descr, description);
}

void da_init_string(char **option, char *opt_name, char *opt_key, char *opt_longk, char *value, char *description) {
    da_init_opt_info(opt_name, opt_key, opt_longk, description);

    da_option[total_options].type = STRING;

    if (value) {
        *option = malloc(strlen(value) + 1);
        strcpy(*option, value);

        da_option[total_options].def_str_val = malloc(strlen(value) + 1);
        strcpy(da_option[total_options].def_str_val, value);
    } else {
        /* without this, a crash if trying to parse an option that has NULL as default value: */
        *option = malloc(0);
        /* also prevent garbage data in some cases: */
        *option = NULL;
    }

    da_option[total_options].str_val = option;
}

void da_init_integer(int *option, char *opt_name, char *opt_key, char *opt_longk, int min_value, int max_value, int value, char *description) {
    da_init_opt_info(opt_name, opt_key, opt_longk, description);
    
    da_option[total_options].type = INTEGER;

    *option = value;

    da_option[total_options].int_val = option;

    da_option[total_options].def_int_val = value;

    da_option[total_options].min_alld_int = min_value;

    da_option[total_options].max_alld_int = max_value;
}

void da_init_float(double *option, char *opt_name, char *opt_key, char *opt_longk, double min_value, double max_value, double value, char *description) {
    da_init_opt_info(opt_name, opt_key, opt_longk, description);

    da_option[total_options].type = FLOATING;

    *option = value;

    da_option[total_options].flt_val = option;

    da_option[total_options].def_flt_val = value;

    da_option[total_options].min_alld_flt = min_value;

    da_option[total_options].max_alld_flt = max_value;
}

void da_init_bool(bool *option, char *opt_name, char *opt_key, char *opt_longk, bool value, char *description) {
    da_init_opt_info(opt_name, opt_key, opt_longk, description);

    da_option[total_options].type = BOOLEAN;
    
    *option = value;

    da_option[total_options].bool_val = option;

    da_option[total_options].def_bool_val = value;
}

void da_log(DALogLevel lev, char *message, ...) {
#ifdef DA_WRITE_LOG
    FILE *F = NULL;

    if ((F = fopen(da_log_file, "a+")) == NULL) {
        printf("WARN: unable to open \"%s\"!\n", da_log_file);
        
        return;
    }

    /* ok, proceed */
    struct tm *time_struct;

    char *buffer;
    char mess_buffer[512];
    char log_level_message[5];

    int bufsize;
    
    long curtime;

    va_list args;

    /* get log level */
    switch (lev) {
    case INFO:
        snprintf(log_level_message, 5, "info");
        break;
        
    case WARN:
        snprintf(log_level_message, 5, "warn");
        break;
        
    case ERR:
        snprintf(log_level_message, 5, "err");
        break;
        
    default:
        break;
    }

    /* get current time */
    curtime = time(0);
    
    time_struct = localtime(&curtime);

    /* get args */
    va_start(args, message);

    vsnprintf(mess_buffer, 512, message, args);

    bufsize = strlen(mess_buffer) + 30 + strlen(da_name);

    buffer = malloc(bufsize);

    snprintf(buffer, bufsize, "[%02i.%02i.%i %02i:%02i:%02i] %s %s: %s", time_struct->tm_mday, time_struct->tm_mon + 1, time_struct->tm_year + 1900, time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec, da_name, log_level_message, mess_buffer);

    fprintf(F, buffer);

    fclose(F);

    va_end(args);

    if (buffer)
        free(buffer);
#endif
}
