#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <stdio.h>
#include <sys/wait.h>

volatile bool da_exec_busy;

/*
 * This is used to execute an external command.
 * This is a heavily stripped-down version of
 * execCommand from WMaker libdockapp and from wmifinfo.
 */
bool da_exec_command(char *);

/*
 * This can be used to perform a certain task inside
 * a loop after the given amount of time in seconds.
 * Returns true if the time has expired,
 * false otherwise.
 */
bool da_timeout(int);

/*
 * Make a string uppercase.
 */
char *da_str_to_upper(char *);
