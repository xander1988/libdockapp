# libdockapp4

This is libdockapp for scaled and/or resized dockapps. It is still alpha; I'm not sure if the implementation is correct. More testing needed.

## Installation

Run ./build.sh (apply --disable-log to disallow writing to log file), then (as root) ./install.sh.
